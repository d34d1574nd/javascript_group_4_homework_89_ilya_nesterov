import axios from '../../axios-api';

export const FETCH_TRACK_HISTORY_SUCCESS = 'FETCH_TRACK_HISTORY_SUCCESS';
export const TRACK_HISTORY = 'TRACK_HISTORY';

const fetchTrackHistorySuccess = tracks => ({type: FETCH_TRACK_HISTORY_SUCCESS, tracks});
const trackHistory = () => ({type: TRACK_HISTORY});

export const usersTrackHistory = () => {
  return (dispatch, getState) => {
    const token = getState().users.user.token;
      return axios.get('/track_history', {headers: {'Authorization': token}}).then(
        response => dispatch(fetchTrackHistorySuccess(response.data))
      )
  }
};

export const userTrackHistory = idTrack => {
  return (dispatch, getState) => {
    const token = getState().users.user.token;
    return axios.post('/track_history', idTrack, {headers: {'Authorization': token}}).then(
      () => dispatch(trackHistory())
    );
  };
};