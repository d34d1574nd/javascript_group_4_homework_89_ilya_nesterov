import axios from '../../axios-api';
import {push} from 'connected-react-router';
import {NotificationManager} from "react-notifications";

export const FETCH_ARTISTS_SUCCESS = 'FETCH_ARTISTS_SUCCESS';
export const LOADING_ARTIST = 'LOADING_ARTIST';

const fetchArtistSuccess = artist => ({type: FETCH_ARTISTS_SUCCESS, artist});
const loadingArtist = cancel => ({type: LOADING_ARTIST, cancel});

export const fetchArtist = () => {
  return dispatch => {
    return axios.get('/artist').then(
      response => dispatch(fetchArtistSuccess(response.data))
    ).finally(
      () => dispatch(loadingArtist(false))
    );
  };
};

export const postArtists = artistData => {
  return dispatch => {
    return axios.post('/artist', artistData).then(
      () => {
        dispatch(fetchArtist());
        NotificationManager.info('Artist created, but not public!');
        dispatch(push('/'))
      }
    )
  }
};

export const postArtistPublic = artistId => {
  return dispatch => {
    return axios.post(`/artist/${artistId}/publish`).then(
      () => {
        dispatch(fetchArtist());
        NotificationManager.success('Artist published!');
      }
    )
  };
};

export const deleteArtist = artistId => {
  return dispatch => {
    return axios.delete(`/artist/${artistId}`).then(
      () => {
        dispatch(fetchArtist());
        NotificationManager.error('Artist deleted!');
      }
    )
  };
};
