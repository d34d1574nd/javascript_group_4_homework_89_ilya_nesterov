import axios from '../../axios-api';
import {push} from 'connected-react-router';
import {NotificationManager}  from 'react-notifications';

export const FETCH_ALBUMS_SUCCESS = 'FETCH_ALBUMS_SUCCESS';
export const LOADING_ALBUM = 'LOADING_ALBUM';

const fetchAlbumsSuccess = albums => ({type: FETCH_ALBUMS_SUCCESS, albums});
const loadingAlbums = cancel => ({type: LOADING_ALBUM, cancel});

export const fetchAlbums = idArtist => {
  return dispatch => {
    return axios.get('/albums?artist=' + idArtist).then(
      response => dispatch(fetchAlbumsSuccess(response.data))
    ).finally(
      () => dispatch(loadingAlbums(false))
    );
  };
};

export const postAlbum = albumData => {
  return dispatch => {
    return axios.post('/albums', albumData).then(
      response => {
        dispatch(fetchAlbums(response.data.artist._id));
        NotificationManager.info('Album created, but not public!');
        dispatch(push(`/albums/${response.data.artist._id}/${response.data.artist.name}`))
      }
    )
  };
};

export const postAlbumPublic = albumData => {
  return dispatch => {
    const albumId = albumData._id;
    return axios.post(`/albums/${albumId}/public`).then(
      () => {
        dispatch(fetchAlbums(albumData.artist._id));
        NotificationManager.success('Album published!');
      }
    )
  };
};

export const deleteAlbum = albumData => {
  return dispatch => {
    const albumId = albumData._id;
    return axios.delete(`/albums/${albumId}`).then(
      () => {
        dispatch(fetchAlbums(albumData.artist._id));
        NotificationManager.error('Album deleted!');
      }
    )
  };
};