import {FETCH_TRACK_HISTORY_SUCCESS} from "../actions/trackHistoryAction";

const initialState = {
  history: []
};

const TrackHistoryReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_TRACK_HISTORY_SUCCESS:
      return {...state, history: action.tracks};
    default:
      return state;
  }
};

export default TrackHistoryReducer;