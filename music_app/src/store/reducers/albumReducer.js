import {FETCH_ALBUMS_SUCCESS, LOADING_ALBUM} from "../actions/albumAction";

const initialState = {
  albums: [],
  loading: true
};

const artistReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_ALBUMS_SUCCESS:
      return {...state, albums: action.albums};
    case LOADING_ALBUM:
      return {...state, loading: action.cancel};
    default:
      return state;
  }
};

export default artistReducer;