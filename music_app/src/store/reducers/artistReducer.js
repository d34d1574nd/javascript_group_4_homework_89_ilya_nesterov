import {FETCH_ARTISTS_SUCCESS, LOADING_ARTIST} from "../actions/artistActions";

const initialState = {
  artist: [],
  loading: true
};

const artistReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_ARTISTS_SUCCESS:
      return {...state, artist: action.artist};
    case LOADING_ARTIST:
      return {...state, loading: action.cancel};
    default:
      return state;
  }
};

export default artistReducer;