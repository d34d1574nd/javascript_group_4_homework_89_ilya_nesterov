import {FECTH_TRACK_SUCCESS} from "../actions/trackAction";


const initialState = {
  tracks: []
};

const trackReducer = (state = initialState, action) => {
  switch (action.type) {
    case FECTH_TRACK_SUCCESS:
      return {...state, tracks: action.tracks};
    default:
      return state;
  }
};

export default trackReducer;