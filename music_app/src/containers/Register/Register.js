import React, {Component, Fragment} from 'react';
import {Alert, Button, Col, Form, FormGroup} from "reactstrap";
import {registerUser} from "../../store/actions/usersActions";
import {connect} from "react-redux";
import FormElement from "../../components/UI/Form/FormElement";
import FacebookLogin from '../../components/FacebookLogin/FacebookLogin';

class Register extends Component {
  state = {
    username: '',
    displayName: '',
    password: '',
    avatar: null
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    })
  };

  fileChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.files[0]
    })
  };

  submitFormHandler = event => {
    event.preventDefault();

    const formData = new FormData();

    Object.keys(this.state).forEach(key => {
      formData.append(key, this.state[key]);
    });

    this.props.registerUser(formData);
  };

  getFieldError = fieldName => {
    return this.props.error && this.props.error.errors && this.props.error.errors[fieldName] && this.props.error.errors[fieldName].message;
  };

  render() {
    return (
      <Fragment>
        <h2>Register new user</h2>
        {this.props.error && this.props.error.global && (
          <Alert color="danger">
            {this.props.error.global}
          </Alert>
        )}

        <Form onSubmit={this.submitFormHandler}>
          <FormGroup>
            <FacebookLogin/>
          </FormGroup>

          <FormElement
            propertyName="username"
            title="Username"
            type="text"
            value={this.state.username}
            onChange={this.inputChangeHandler}
            error={this.getFieldError('username')}
            placeholder="Enter your desired username"
            autoComplete='new-username'
          />

          <FormElement
            propertyName="displayName"
            title="Display name"
            type="text"
            value={this.state.displayName}
            onChange={this.inputChangeHandler}
            error={this.getFieldError('displayName')}
            placeholder="Enter your display Name"
            autoComplete='new-displayName'
          />

          <FormElement
            propertyName="password"
            title="Password"
            type="password"
            value={this.state.password}
            onChange={this.inputChangeHandler}
            error={this.getFieldError('password')}
            placeholder="Enter your secure password"
            autoComplete='new-password'
          />

          <FormElement
            propertyName="avatar"
            title="Avatar" required={true}
            type="file"
            onChange={this.fileChangeHandler}
          />


          <FormGroup row>
            <Col sm={{offset: 2, size: 10}}>
              <Button type="submit" color="primary">
                Register
              </Button>
            </Col>
          </FormGroup>
        </Form>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  error: state.users.registerError
});

const mapDispatchToProps = dispatch => ({
  registerUser: userData => dispatch(registerUser(userData))
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);
