import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {deleteAlbum, fetchAlbums, postAlbumPublic} from "../../store/actions/albumAction";
import {Button, ButtonGroup, Card, CardBody, CardFooter, CardText, CardTitle, Col, Row} from "reactstrap";
import ParamThumbnail from "../../components/ParamThumbnail/ParamThumbnail";
import {NavLink} from "react-router-dom";
import Spinner from "../../components/UI/Spinner/Spinner";

class AlbumPage extends Component {
  componentDidMount() {
    this.props.getAlbums(this.props.match.params.id);
  }

  goBack = () => {
    this.props.history.goBack('/');
  };

  render() {
    if (this.props.loading) {
      return <Spinner/>
    }

    const album = this.props.album.map(albums => {
      return (
          <Col sm="4" key={albums._id}>
            <Card body outline color={albums.public === true ? "primary" : "danger"}>
              <strong>Artist</strong>
              <CardTitle>{albums.artist.name}
              </CardTitle>
              <ParamThumbnail
                param='album'
                image={albums.photoAlbum}
              />
              <CardBody>
                <strong>Album title</strong>
                <CardTitle>{albums.titleAlbum}</CardTitle>
                <strong>Year</strong>
                <CardText>{albums.year}</CardText>
              </CardBody>
              {
                albums.public !== false ?
                  <CardFooter className="text-muted">
                    <NavLink to={`/tracks/${albums._id}/${albums.titleAlbum}/${albums.artist.name}`}>Go to tracks</NavLink>
                    {this.props.user && this.props.user.role === 'admin' ?
                      <Button style={{borderRadius: '50%', padding: '0 7px', float: 'right'}} onClick={() => this.props.deleteAlbum(albums)} outline color="danger"><i className="fas fa-times" /></Button>
                      : null
                    }
                  </CardFooter> :
                  <CardFooter className="text-muted">
                    <span>Not published</span>
                    {this.props.user && this.props.user.role === 'admin' ?
                      <ButtonGroup style={{float: 'right'}}>
                        <Button style={{padding: '0 7px'}} onClick={() => this.props.postAlbumPublic(albums)} outline color="success">Public</Button>
                        <Button style={{padding: '0 7px'}} onClick={() => this.props.deleteAlbum(albums)} outline color="danger">Delete</Button>
                      </ButtonGroup>
                      : null}
                  </CardFooter>
              }
            </Card>
          </Col>
        )
      }
    );
    return (
      <Fragment>
            <Button color="link" onClick={this.goBack}>Go back</Button>
        <Row>
          {album}
        </Row>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  album: state.album.albums,
  loading: state.album.loading,
  user: state.users.user
});

const mapDispatchToProps = dispatch => ({
  getAlbums: idArtist => dispatch(fetchAlbums(idArtist)),
  postAlbumPublic: albumData => dispatch(postAlbumPublic(albumData)),
  deleteAlbum: albumData => dispatch(deleteAlbum(albumData))
});

export default connect(mapStateToProps, mapDispatchToProps)(AlbumPage);