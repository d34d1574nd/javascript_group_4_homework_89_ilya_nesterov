import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {deleteTrack, fetchTrack, postTrackPublic} from "../../store/actions/trackAction";
import {Button, Card, CardHeader, Col, Modal, ModalBody, ModalHeader, Row} from "reactstrap";

import './TrackPage.css';
import {userTrackHistory} from "../../store/actions/trackHistoryAction";
import {NavLink} from "react-router-dom";

class TrackPage extends Component {
  state = {
    showClip: null
  };

  componentDidMount() {
    this.props.getTrack(this.props.match.params.id);
  }

  goBack = () => {
    this.props.history.goBack('/albums');
  };

  showModal = track => {
    this.setState({showClip: track})
  };

  hideModal = () => {
    this.setState({showClip: null});
  };

  render() {
    return (
      <Fragment>
        <Button  color="link" onClick={this.goBack}>Go back</Button>
        <Row>
          <Col  md="8">
            <Card>
              <CardHeader>
                <Row>
                  <Col sm="4">
                    <strong>artist: </strong>{this.props.match.params.artist}
                  </Col>
                  <Col sm="4">
                    <strong>album: </strong>{this.props.match.params.album}
                  </Col>
                </Row>
              </CardHeader>
            </Card>
          </Col>
        </Row>
        <Row>
          {this.props.track.map(track => (
            <Col sm="8" key={track._id}>

              <Card body outline color={track.public === true ? "primary" : "danger"}>
                <Row>
                  {this.props.user && this.props.user.role === 'admin' ?
                    <Col sm='1'>
                      <Button style={{borderRadius: '50%', padding: '0 7px'}} onClick={() => this.props.deleteTrack(track)} outline color="danger">
                        <i className="fas fa-times" />
                      </Button>
                    </Col> : null}
                  <Col sm="1"><strong>#{track.numberSong}</strong></Col>
                  <Col sm="5"><span>{track.titleSong}</span></Col>
                  <Col sm="3"><strong>{track.duration}</strong></Col>
                  {track.public === false ?
                    <Col sm={{offset:6, size: 6}}>
                      <strong>Not published</strong>
                      {this.props.user && this.props.user.role === 'admin' ?
                          <Button style={{float: 'right', padding: '0 7px'}} onClick={() => this.props.postTrackPublic(track)} outline color="success">Public</Button> : null}
                    </Col>
                  : this.props.user ? <Fragment>
                      <Col sm='1'>
                        <Button outline color="danger" className="play" onClick={() => this.props.userTrackHistory({track: track._id})}>
                          <i className="fas fa-play" />
                        </Button>
                      </Col>
                      {track.url ? (
                        <Col sm="1">
                          <Button color="danger" onClick={() => this.showModal(track)} className='button'>
                            <i className="fab fa-youtube"/>
                          </Button>
                        </Col>) : null}
                    </Fragment>: <Col><NavLink to="/login"><i className="far fa-times-circle" style={{fontSize: '25px'}}/></NavLink></Col>}

                </Row>
              </Card>
            </Col>))}

          <Modal isOpen={!!this.state.showClip} toggle={this.hideModal} >
            {this.state.showClip && (
              <Fragment>
                <ModalHeader toggle={this.hideModal}><strong>{this.props.match.params.artist}: </strong>{this.state.showClip.titleSong}</ModalHeader>
                <ModalBody>
                  <iframe
                    width="560" height="315" title={this.state.showClip.titleSong}
                    src={this.state.showClip.url}
                    frameBorder="0"
                    allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                    allowFullScreen />
                </ModalBody>
              </Fragment>
            )}
          </Modal>
        </Row>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  track: state.track.tracks,
  user: state.users.user
});

const mapDispatchToProps = dispatch => ({
  getTrack: idAlbum => dispatch(fetchTrack(idAlbum)),
  userTrackHistory: idTrack => dispatch(userTrackHistory(idTrack)),
  postTrackPublic: track => dispatch(postTrackPublic(track)),
  deleteTrack: track => dispatch(deleteTrack(track))
});

export default connect(mapStateToProps, mapDispatchToProps)(TrackPage);