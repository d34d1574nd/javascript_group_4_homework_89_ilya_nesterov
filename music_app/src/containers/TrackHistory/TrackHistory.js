import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {usersTrackHistory} from "../../store/actions/trackHistoryAction";
import {Card, CardText, CardTitle} from "reactstrap";
import {Redirect} from "react-router-dom";

class TrackHistory extends Component {
  componentDidMount() {
    if (!this.props.user) return <Redirect to="/login"/>;
    this.props.trackHistory();
  };

  render() {
    if (!this.props.user) return <Redirect to="/login"/>;
    return (
      <Fragment>
        {this.props.history.map(tracks => (
          <Card body outline color="success" key={tracks._id}>
            <CardTitle><strong>Artist: </strong>{tracks.track.album.artist.name}</CardTitle>
            <CardTitle><strong>Track: </strong>{tracks.track.titleSong}</CardTitle>
            <CardText><strong>date: </strong>{tracks.datetime}</CardText>
          </Card>
        ))}
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  history: state.history.history,
  user: state.users.user,

});

const mapDispatchToProps = dispatch => ({
  trackHistory: () => dispatch(usersTrackHistory())
});

export default connect(mapStateToProps, mapDispatchToProps)(TrackHistory);