import React from 'react';
import {DropdownItem, DropdownMenu, DropdownToggle, UncontrolledDropdown} from "reactstrap";
import {NavLink} from "react-router-dom";
import AvatarThumbnail from "../../../AvatarThumbnail/AvatarThumbnail";

const UserMenu = ({user, logout}) => (
  <UncontrolledDropdown nav inNavbar>
    <DropdownToggle nav caret>
      Hello,
      {user.avatarUrl ? <img src={user.avatarUrl} className='userAvatar' alt='user'/> : <AvatarThumbnail param='avatar' image={user.avatar}/>}
      {user.role === 'admin' ? <strong>admin </strong> : null} {user.displayName}
    </DropdownToggle>
    <DropdownMenu right>
      <DropdownItem>
        <NavLink to='/add_artist'>Add artist</NavLink>
      </DropdownItem>
      <DropdownItem>
        <NavLink to='/add_album'>Add album</NavLink>
      </DropdownItem>
      <DropdownItem>
        <NavLink to='/add_track'>Add track</NavLink>
      </DropdownItem>
      <DropdownItem divider />
      <DropdownItem>
        <NavLink to='/track_history'>Track history</NavLink>
      </DropdownItem>
      <DropdownItem divider />
      <DropdownItem onClick={logout}>
        Log out
      </DropdownItem>
    </DropdownMenu>
  </UncontrolledDropdown>
);

export default UserMenu;