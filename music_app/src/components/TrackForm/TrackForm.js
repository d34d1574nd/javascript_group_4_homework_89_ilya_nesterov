import React, {Component, Fragment} from 'react';
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";
import {fetchAlbums} from "../../store/actions/albumAction";
import {fetchArtist} from "../../store/actions/artistActions";
import {connect} from "react-redux";
import FormElement from "../UI/Form/FormElement";
import {postTrack} from "../../store/actions/trackAction";

class TrackForm extends Component {
  state = {
    artist: '',
    numberSong: '',
    titleSong: '',
    album: '',
    duration: '',
    url: '',
  };

  componentDidMount() {
    this.props.getArtist();
  };

  submitFormHandler = event => {
    event.preventDefault();
    this.props.postTracks({...this.state});
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });

  };

  render() {
    return (
      <Fragment>
        <Form onSubmit={this.submitFormHandler}>

          <FormGroup row>
            <Label sm={2} for="artist">Artist</Label>
            <Col sm={10}>
              <Input
                type="select" required
                name="artist" id="artist"
                value={this.state.artist}
                onChange={this.inputChangeHandler}
                onClick={() => this.props.getAlbums(this.state.artist)}
              >
                <option value="">Please select artist...</option>
                {this.props.artist.map(artist => (
                  <option key={artist._id} value={artist._id} >{artist.name}</option>
                ))}
              </Input>
            </Col>
          </FormGroup>

          {this.state.artist ?
            <FormGroup row>
            <Label sm={2} for="album">Albums</Label>
            <Col sm={10}>
              <Input
                type="select" required
                name="album" id="album"
                value={this.state.album}
                onChange={this.inputChangeHandler}
              >
                <option value="">Please select album...</option>
                {this.props.album.map(album => (
                  <option key={album._id} value={album._id}>{album.titleAlbum}</option>
                ))}
              </Input>
            </Col>
          </FormGroup> : null}

          <FormElement
            propertyName="numberSong"
            onChange={this.inputChangeHandler}
            title="Enter number song in album"
            value={this.state.numberSong}
            type='number' required={true}
          />

          <FormElement
            propertyName='titleSong'
            title="Enter title song in album"
            type="text" value={this.state.titleSong}
            onChange={this.inputChangeHandler}
            required={true}
          />

          <FormElement
            propertyName="duration"
            title="Enter duration song"
            type="text" required={true}
            value={this.state.duration}
            onChange={this.inputChangeHandler}
          />

          <FormElement
            propertyName="url"
            title="Enter url song on Youtube"
            type="text"
            value={this.state.url}
            onChange={this.inputChangeHandler}
          />

          <FormGroup row>
            <Col sm={{offset:2, size: 10}}>
              <Button type="submit" color="primary">Save</Button>
            </Col>
          </FormGroup>
        </Form>
      </Fragment>
    );
  }
}


const mapStateToProps = state => ({
  artist: state.artist.artist,
  album: state.album.albums
});

const mapDispatchToProps = dispatch => ({
  postTracks: track => dispatch(postTrack(track)),
  getAlbums: idArtist => dispatch(fetchAlbums(idArtist)),
  getArtist: () => dispatch(fetchArtist())
});

export default connect(mapStateToProps, mapDispatchToProps)(TrackForm);