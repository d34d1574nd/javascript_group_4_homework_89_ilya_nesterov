import React, {Component, Fragment} from 'react';
import {Button, Col, Form, FormGroup} from "reactstrap";
import FormElement from "../UI/Form/FormElement";
import {connect} from "react-redux";
import {postArtists} from "../../store/actions/artistActions";

class ArtistForm extends Component {
  state = {
    name: '',
    photo: null,
    information: ''
  };

  submitFormHandler = event => {
    event.preventDefault();

    const formData = new FormData();

    Object.keys(this.state).forEach(key => {
      formData.append(key, this.state[key]);
    });

    this.props.postArtists(formData);
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  fileChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.files[0]
    })
  };

  render() {
    return (
      <Fragment>
        <Form onSubmit={this.submitFormHandler}>
          <FormElement
            title="Artist name (group)" type="text"
            value={this.state.name}
            propertyName="name"
            onChange={this.inputChangeHandler}
            required={true} placeholder="Enter artist name(group)"
          />
          <FormElement
            propertyName="information"
            value={this.state.information}
            title="Information" type='textarea'
            onChange={this.inputChangeHandler}
            placeholder="Enter information about artist(group)"
          />
          <FormElement
            propertyName="photo"
            title="Photo" required={true}
            type="file"
            onChange={this.fileChangeHandler}
          />
          <FormGroup row>
            <Col sm={{offset:2, size: 10}}>
              <Button type="submit" color="primary">Save</Button>
            </Col>
          </FormGroup>
        </Form>
      </Fragment>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  postArtists: artistData => dispatch(postArtists(artistData)),
});

export default connect(null, mapDispatchToProps)(ArtistForm);