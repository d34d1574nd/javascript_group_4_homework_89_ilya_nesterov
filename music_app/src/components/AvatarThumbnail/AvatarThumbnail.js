import React from 'react';

import {apiURL} from "../../constants";

import './AvatarThumbnail.css';

const AvatarThumbnail = props => {
  let image = null;

  if (props.image) {
    image = apiURL + '/uploads/' + props.param + '/' + props.image;
  }

  return <img src={image} className='userAvatar' alt="Avatar" />
};

export default AvatarThumbnail;