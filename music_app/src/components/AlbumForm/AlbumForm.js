import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";
import {Button, Col, Form, FormGroup, Input, Label} from "reactstrap";
import FormElement from "../UI/Form/FormElement";
import {postAlbum} from "../../store/actions/albumAction";
import {fetchArtist} from "../../store/actions/artistActions";

class AlbumForm extends Component {
  state = {
    titleAlbum: '',
    artist: '',
    year: '',
    photoAlbum: null
  };

  componentDidMount() {
    this.props.getArtist()
  }

  submitFormHandler = event => {
    event.preventDefault();

    const formData = new FormData();

    Object.keys(this.state).forEach(key => {
      formData.append(key, this.state[key]);
    });

    this.props.postAlbums(formData);
  };

  inputChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.value
    });
  };

  fileChangeHandler = event => {
    this.setState({
      [event.target.name]: event.target.files[0]
    })
  };

  render() {
    return (
      <Fragment>
        <Form onSubmit={this.submitFormHandler}>
          <FormGroup row>
            <Label sm={2} for="artist">Artist</Label>
            <Col sm={10}>
              <Input
                type="select" required
                name="artist" id="artist"
                value={this.state.artist}
                onChange={this.inputChangeHandler}
              >
                <option value="">Please select artist...</option>
                {this.props.artist.map(artist => (
                  <option key={artist._id} value={artist._id}>{artist.name}</option>
                ))}
              </Input>
            </Col>
          </FormGroup>

          <FormElement
            type='text' propertyName="titleAlbum"
            title="Album title"
            value={this.state.titleAlbum}
            onChange={this.inputChangeHandler}
            placeholder="Enter album title"/>

         <FormElement
            type="text" title="Year album"
            value={this.state.year}
            onChange={this.inputChangeHandler}
            placeholder="Enter year album"
            propertyName="year"
         />

         <FormElement
           propertyName="photoAlbum"
           title="Photo album"
           type='file'
           onChange={this.fileChangeHandler}
         />

          <FormGroup row>
            <Col sm={{offset:2, size: 10}}>
              <Button type="submit" color="primary">Save</Button>
            </Col>
          </FormGroup>

        </Form>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  artist: state.artist.artist,
});

const mapDispatchToProps = dispatch => ({
  postAlbums: albumData => dispatch(postAlbum(albumData)),
  getArtist: () => dispatch(fetchArtist())
});

export default connect(mapStateToProps, mapDispatchToProps)(AlbumForm);