const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const Album = require('../models/Album');
const config = require("../config");
const auth = require('../middleware/auth');
const tryAuth = require('../middleware/tryAuth');
const permit = require('../middleware/permit');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPathAlbum);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', tryAuth, async (req, res) => {
  try {
    let criteria = {public: true, artist: req.query.artist};

    if (req.user) {
      criteria  = {
        artist: req.query.artist,
        $or: [
          {public: true},
          {user: req.user}
        ]
      }
    }
    if(req.user && req.user.role === 'admin') {
      criteria  = {
        artist: req.query.artist,
        $or: [
          {public: true},
          {public: false},
          {user: req.user}
        ]
      }
    }

    const album = await Album.find(criteria).populate('artist', '_id, name');

    res.send(album);
  } catch(e) {
    if(e.name === 'ValidationError') {
      return res.status(400).send(e)
    }
    return res.status(500).send(e)
  }

});

router.post('/', auth, upload.single('photoAlbum'), (req, res) => {
  const albumInfo = {...req.body, user: req.user._id};

  if (req.file) {
    albumInfo.photoAlbum = req.file.filename;
  }

  const album = new Album(albumInfo);

  album.save()
    .then(result => Album.populate(result, 'artist').then(result => res.send(result)))
    .catch(error => res.status(400).send(error))
});

router.get('/:id', (req, res) => {
  Album.findById(req.params.id).populate('artist', '_id, name')
    .then(album => {
      if (album) res.send(album);
      else res.sendStatus(404);
    })
    .catch(() => res.sendStatus(500));
});

router.post('/:id/public', [auth, permit('admin')], async (req, res) => {
  const album = await Album.findById(req.params.id);

  if(!album) {
    return res.sendStatus(404);
  }

  album.public = !album.public;

  await album.save();

  return res.send(album);
});

router.delete('/:id', [auth, permit('admin')], (req, res) => {
  Album.remove({_id: req.params.id})
    .then(result => res.send(result))
    .catch(error => res.status(403).send(error))
});

module.exports = router;