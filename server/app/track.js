const express = require('express');
const Track = require('../models/Track');
const auth = require('../middleware/auth');
const tryAuth = require('../middleware/tryAuth');
const permit = require('../middleware/permit');

const router = express.Router();

router.get('/', tryAuth, async (req, res) => {
  try {
    let criteria = {public: true, album: req.query.album};

    if (req.user) {
      criteria  = {
        album: req.query.album,
        $or: [
          {public: true},
          {user: req.user},
        ]
      }
    }
    if(req.user && req.user.role === 'admin') {
      criteria  = {
        album: req.query.album,
        $or: [
          {public: true},
          {public: false},
          {user: req.user}
        ]
      }
    }

    const track = await Track.find(criteria).sort({numberSong: 1}).populate('album', 'titleAlbum');
    res.send(track);
  } catch(e) {
    if(e.name === 'ValidationError') {
      return res.status(400).send(e)
    }
    return res.status(500).send(e)
  }

});

router.post('/', [auth, permit('admin', 'user')], (req, res) => {
  const trackInfo = {...req.body, user: req.user._id};

  const tracks = new Track(trackInfo);

  tracks.save()
    .then(result => Track.populate(result, {path: 'album', populate: {path: 'artist'}})).then(result => res.send(result))
    .catch(error => res.status(400).send(error));
});

router.post('/:id/public', [auth, permit('admin')], async (req, res) => {
  const track = await Track.findById(req.params.id);

  if(!track) {
    return res.sendStatus(404);
  }

  track.public = !track.public;

  await track.save();

  return res.send(track);
});

router.delete('/:id', [auth, permit('admin')], (req, res) => {
  Track.deleteOne({_id: req.params.id})
    .then(result => res.send(result))
    .catch(error => res.status(403).send(error))
});

module.exports = router;