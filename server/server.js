const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const config = require('./config');

const artist = require('./app/artist');
const album = require('./app/album');
const track = require('./app/track');
const user = require('./app/users');
const trackHistory = require('./app/trackHistory');

const app = express();
app.use(cors());
app.use(express.json());
app.use(express.static('public'));

const port = 8000;

mongoose.connect(config.dbUrl, config.mongoOptions).then(() => {
  app.use('/artist', artist);
  app.use('/albums', album);
  app.use('/tracks', track);
  app.use('/users', user);
  app.use('/track_history', trackHistory);

  app.listen(port, () => {
    console.log(`Server started on ${port} port`);
  });
});
