const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const AlbumSchema = new Schema ({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  titleAlbum: {type: String, required: true},
  artist: {
    type: Schema.Types.ObjectId,
    ref: 'Artist',
    required: true
  },
  year: {type: String, required: true},
  photoAlbum: {type: String, required: true},
  public: {
    type: Boolean,
    required: true,
    default: false
  }
});

const Album = mongoose.model('Album', AlbumSchema);

module.exports = Album;