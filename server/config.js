const path = require('path');

const rootPath = __dirname;

module.exports = {
  rootPath,
  uploadPathArtist: path.join(rootPath, 'public/uploads/artist'),
  uploadPathAlbum: path.join(rootPath, 'public/uploads/album'),
  uploadPathAvatar: path.join(rootPath, 'public/uploads/avatar'),
  dbUrl: 'mongodb://localhost/melomanFM',
  mongoOptions: {
    useNewUrlParser: true,
    useCreateIndex: true
  },
  facebook: {
    appId: '2393901570838227',
    appSecret: '1d52c1f89c5313ae8dd7566062d14655'
  }
};
